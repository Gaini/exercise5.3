const calculator = require('../src/calculator');
const expect = require('chai').expect;

describe('Задача 1', () => {

    it('calculator is defined', () => {
        expect(calculator).is.not.undefined;
    });

    it('calculator has add', () => {
        expect(calculator.add).is.not.undefined;
    });

    it('calculator.add(",1") = 1', () => {
        expect(calculator.add(",1")).to.be.equal(1);
    });

    it('calculator.add(",1,2") = 3', () => {
        expect(calculator.add(",1,2")).to.be.equal(3);
    });

    it('calculator.add("") = 0 - default', () => {
        expect(calculator.add("")).to.be.equal(0);
    });

});

describe('Задача 3', () => {

    it('calculator.add("1\\n2,3") = 0 - default', () => {
        expect(calculator.add("1\n2,3")).to.be.equal(6);
    });

    it('calculator.add("1,\\n") = 0 - default', () => {
        expect(() => calculator.add("1,\n")).to.throws('not valid input');
    });

    it('calculator.add("1\\n,") = 0 - default', () => {
        expect(() => calculator.add("1\n,")).to.throws('not valid input');
    });

});

describe('Задача 4', () => {

    it('calculator.add("//<;>\\n1;2;3;;4") = 10', () => {
        expect(calculator.add("//<;>\n1;2;3;;4")).to.be.equal(10);
    });

    it('calculator.add("//<_>\\n1_2_3__4") = 10', () => {
        expect(calculator.add("//<_>\n1_2_3__4")).to.be.equal(10);
    });

    it('calculator.add("//<+>\\n1+2+3+4") = 10', () => {
        expect(calculator.add("//<+>\n1+2+3+4")).to.be.equal(10);
    });

});

describe('Задача 5', () => {

    it('calculator.add("//<+>\\n-1+2") One negative value ', () => {
        expect(() => calculator.add("//<+>\n-1+2")).to.throws('Negative value not allowed "-1"');
    });

    it('calculator.add("//<+>\\n4-1+-2") Few negative values ', () => {
        expect(() => calculator.add("//<+>\n4+-1+-2")).to.throws('Negative value not allowed "-1,-2"');
    });

});

/*
*
    Числа больше 1000 должны быть проигнорированы. Выражение 2 + 1001 будет равно 2.
    Разделители могут быть любой длины при условии соблюдения формата //<разделитель>\n. Например, //<***>\n1***2***3 должно вернуть 6.
    Добавьте возможность использовать разные разделители, указанные в формате //<разделитель1><разделитель2>\n. К примеру, //<*><%>\n1*2%3 должно вернуть 6.
    Убедитесь, что вы можете поддерживать сценарии, в которых участвует любое количество разделителей с любым количеством символов в каждом.

* */

describe('Задача 6', () => {

    it('calculator.add("//<+>\\n2+1001") Ignore values, more than 1000 ', () => {
        expect(calculator.add("//<+>\n2+1001")).to.be.equal(2);
    });

    it('calculator.add("//<+++>\\n1+++2+++3+++4") Use big delimiter ', () => {
        expect(calculator.add("//<+++>\n1+++2+++3+++4")).to.be.equal(10);
    });

    it('calculator.add("//<+><;>\\n1+2;3+4") Use few delimiters ', () => {
        expect(calculator.add("//<+><;>\n1+2;3+4")).to.be.equal(10);
    });

    it('calculator.add("//<++><;;>\\n1++2;;3++4") Use few delimiters with few symbols ', () => {
        expect(calculator.add("//<++><;;>\n1++2;;3++4")).to.be.equal(10);
    });

});