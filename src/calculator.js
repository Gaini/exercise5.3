exports.add = (inputSrt) => {

    let separator = /[,\n]/,
        arraySeparator = [],
        stringSeparator = '[,\\n';

    if (/\/\/<.*>\n/.test(inputSrt)) {
        separator = inputSrt.substring(inputSrt.indexOf("<") + 1, inputSrt.lastIndexOf(">"));
        inputSrt = inputSrt.slice(inputSrt.lastIndexOf(">") + 2);
        arraySeparator.push(separator.split('><'));

    } else if (/(,\n)|(\n,)/.test(inputSrt)) {
        throw new Error('not valid input');
    }

    if (arraySeparator.length > 0) {
        stringSeparator += arraySeparator.join();
    }
    stringSeparator += ']';

    var regEx = new RegExp(stringSeparator, 'gi');
    const numbers = inputSrt.split(regEx);

    const negativeNumbers = numbers.filter(function (number) {
        return number < 0;
    });
    if (negativeNumbers.length > 0) throw new Error('Negative value not allowed "' + negativeNumbers.join() + '"');
    return numbers.reduce((result, number) => result + ((parseInt(number) > 1000 ? 0 : parseInt(number)) || 0), 0);
};